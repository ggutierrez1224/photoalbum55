package model;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Class for photo tags
 * @author Gabriel Gutierrez and Lyle Filonuk
 */

public class Tag implements Comparator<Tag>, Comparable<Tag>, Serializable
{
    //static final long serialVersionUID = 1L;

    /**
     * type of tag
     */
    private String tagType;
    /**
     * value of tag
     */
    private String tagValue;

    /**
     * Constructor for a tag object.
     * @param tagType, type to set the tag to
     * @param tagValue, value to set the tag to
     */
    public Tag(String tagType, String tagValue)
    {
        this.tagType = tagType;
        this.tagValue = tagValue;
    }

    /**
     * Returns the type of the tag
     * @return the type of the tag
     */
    public String getType()
    {
        return tagType;
    }

    /**
     * Returns the value of the tag
     * @return the value of the tag
     */
    public String getValue()
    {
        return this.tagValue;
    }

    /**
     * Sets the type of the tag
     * @param tagType, type to set tag to
     */
    public void setType(String tagType)
    {
        this.tagType = tagValue;
    }

    /**
     * Sets the value of the tag
     * @param tagValue, value to set tag to
     */
    public void setValue(String tagValue)
    {
        this.tagValue = tagValue;
    }

    /**
     * Checks if this tag is equal to another tag
     * @param  o, object to check
     * @return true if tags are equal, false otherwise
     */
    @Override
    public boolean equals(Object o)
    {
        //make sure param is instance of Tag
        if (o == null || !(o instanceof Tag))
        {
            return false;
        }
        //cast to Tag object
        Tag ot = (Tag) o;

        //if no type, check if values are equal
        if (ot.getType().equals(""))
        {
            return this.getValue().toLowerCase().equals(ot.getValue().toLowerCase());
        }
        //if no vale, check if types are equal
        if (ot.getValue().equals(""))
        {
            return this.getType().toLowerCase().equals(ot.getType().toLowerCase());
        }
        //otherwise return string comparison between type and value of both objects
        return (this.getType().toLowerCase().equals(ot.getType().toLowerCase())
                && this.getValue().toLowerCase().equals(ot.getValue().toLowerCase()));
    }

    /**
     * Used for sorting a collection of tags.
     * @param t1, tag object being compared
     * @param t2, tag object being compared
     * @return value returned from compareTo method
     */
    @Override
    public int compare(Tag t1, Tag t2)
    {
        return t1.compareTo(t2);
    }

    /**
     * Compares this tag to another tag. First compares the type of the tag, if types are equal, then tag values are compared.
     * @param t, tag to be compared to
     * @return value returned from string compareTo method
     */
    @Override
    public int compareTo(Tag t)
    {
        if (this.getType().toLowerCase().equals(t.getType().toLowerCase()))
        {
            return this.getValue().toLowerCase().compareTo(t.getValue().toLowerCase());
        }
        else
        {
            return this.getType().toLowerCase().compareTo(t.getType().toLowerCase());
        }
    }
}
