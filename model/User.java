package model;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable
{
    //private static final long serialVersionUID = 1L;

    /**
     * user's name
     */
    private String name;
    /**
     * user's login username
     */
    private String username;
    /**
     * user's login password
     */
    private String password;
    /**
     * list of user's albums
     */
    private ArrayList<Album> albums;

    /**
     * constructor for user
     * @param username, login username
     * @param password, login password
     */
    public User(String name, String username, String password)
    {
        this.name = name;
        this.username = username;
        this.password = password;
        albums = new ArrayList<Album>();
    }

    /**
     * adds an album to the user's album list
     * @param albName, album name
     */
    public void addAlbum(String albName)
    {
        Album album = new Album(albName, this);
        //check for null list
        if (albums == null)
        {
            albums = new ArrayList<Album>();
            albums.add(album);
        }
        //list is not null
        else
        {
            //search list by name
            for(Album a : albums)
            {
                //make sure album name doesn't already exist
                if(a.getAlbumName().equals(album.getAlbumName()))
                {
                    Backend.errorPrompt("Album Add Failed", "Cannot add album, name already being used!");
                    return;
                }
            }
            //album name is not in use, add album
            albums.add(album);
        }

    }

    /**
     * removes an album from user's album list
     * @param album, album to be removed
     */
    public boolean removeAlbum(Album album)
    {
        //check for null list
        if (albums == null)
        {
            return false;
        }
        //check if list contains albumm to be removed
        for(Album a : albums)
        {
            //if album is in the list, remove it
            if(a.getAlbumName().equals(album.getAlbumName()))
            {
                albums.remove(album);
                return true;
            }
        }
        //album does not exist, prompt user
        Backend.errorPrompt("Album Remove Failed", "Album cannot be removed, it does not exist!");
        return false;
    }

    /**
     * returns album from list with specified name
     * @param name, name of album being searched
     * @return album with name matching parameter name
     */
    public Album getAlbum (String name)
    {
        //search list for album
        for(Album a : albums)
        {
            //if name matches, return it
            if(a.getAlbumName().equals(name))
            {
                return a;
            }
        }
        //album not found, prompt user
        Backend.errorPrompt("Album Search Error", "Album does not exist!");
        return null;
    }

    /**
     * checks if album name exists
     * @param aName, name to be checked
     * @return true if name exists, false otherwise
     */
    public boolean checkName(String aName)
    {
        for(Album a : albums)
        {
            //if name matches, return it
            if(a.getAlbumName().equals(aName))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * get user's name
     * @return name of user
     */
    public String getName()
    {
        return name;
    }

    /**
     * gets user's username
     * @return username
     */
    public String getUsername()
    {
        return  username;
    }

    /**
     * get user's password
     * @return password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * gets list of user's albums
     * @return album list
     */
    public ArrayList<Album> getAlbums()
    {
        if(albums == null)
        {
            return null;
        }
        return albums;
    }

    /**
     * sets user's username
     * @param username, username to be set
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * sets user's password
     * @param password, passowrd to be set
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * sets user's name
     * @param name, name to be set for user
     */
    public void setName(String name)
    {
        this.name = name;
    }
}