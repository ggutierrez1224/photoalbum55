package model;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import app.PhotoAlbum;

/**
 * @author Gabriel Gutierrez, Lyle Filonuk
 *
 * Stores all of the data for the application. Holds all the methods for creating new data, deleting new data,
 * serializing data, and deserializing data.
 */
public class Backend
{   
    //static final long serialVersionUID = 1L;

    /**
     * list of users
     */
	public static ArrayList<User> users = new ArrayList<User>();
    /**
     * directory of data file
     */
	private static final String dir = "data";
    /**
     * name of data file
     */
    private static final String filename = "data.dat";

    /**
     * Admin Controller will call this to add a new user from the backend ArrayList.
     */
    public static void addUser(String newUser) {
    	users.add(new User(newUser, newUser, newUser));
    }

    /**
     * Admin Controller will call this to delete a user from the backend ArrayList.
     */
    public static void deleteUser(int index) {
    	users.remove(index);
    }

    /**
     * get directory of data file
     * @return directory name
     */
    public static String getDir()
    {
        return dir;
    }

    /**
     * get data file name
     * @return file name
     */
    public static String getFilename()
    {
        return filename;
    }

    /**
     * returns list of user
     * @return user list
     */
    public static ArrayList<User> getUsers()
    {
        return users;
    }

    /**
     * confirmation prompt
     * @param header, header for prompt
     * @param content, prompt message
     * @return user decision
     */
    public static boolean confirm(String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText(header);
        alert.setContentText(content);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * prompts user for input
     * @param title, prompt title
     * @param header, prompt header
     * @param content, prompt mesage
     * @return user's input
     */
    public static String inputPrompt(String title, String header, String content)
    {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText(content);

        Optional<String> result = dialog.showAndWait();
        String ret = "";
        if (result.isPresent())
        {
           ret = result.get();
        }
        return ret;
    }

    /**
     * prompts use of error
     * @param header, prompt header
     * @param content, prompt message
     */
    public static void errorPrompt(String header, String content)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.showAndWait();
    }
    
    /**
     * Saves project
     *
     */
    public static void save()
    {

        File file = new File(getDir());
        boolean isDirectoryCreated = file.exists();
        if (!isDirectoryCreated)
        {
            isDirectoryCreated = file.mkdir();
        }
        try
        {
            if(isDirectoryCreated)
            {
                FileOutputStream fo = new FileOutputStream(getDir() + File.separator + getFilename());
                ObjectOutputStream out = new ObjectOutputStream(fo);
                out.writeObject(users);
                out.close();
                fo.close();
                //System.out.println("User list saved.");
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Loads Project
     */
    public static void loadProject()
    {
        try
        {
            FileInputStream fis = new FileInputStream(getDir() + File.separator + getFilename());
            ObjectInputStream ois = new ObjectInputStream(fis);
            users = (ArrayList<User>) ois.readObject();
            if (users == null)
            {
                users = new ArrayList<User>();
            }
            ois.close();
            fis.close();
            //System.out.println("User list loaded");
        }
        catch(FileNotFoundException e)
        {
            users = new ArrayList<User>();
            //System.out.println("User list created");
        }
        catch(ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
