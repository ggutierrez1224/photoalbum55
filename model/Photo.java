package model;
/**
 * Class to create photo objects
 * @author Gabriel Gutierrez and Lyle Filonuk
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class Photo implements Serializable, Comparable<Photo>
{
    static final long serialVersionUID = 1L;

    /**
     * user given name for the photo
     */
    private String photoName;
    /**
     * path to photo
     */
    private String path;
    /**
     * caption of the photo
     */
    private String caption;
    /**
     * date to be used
     */
    private Calendar date;
    /**
     * list of albums photo is in
     */
    private ArrayList<Album> albumsIn;
    /**
     * list of tags photo has
     */
    private ArrayList<Tag> photoTags;

    /**
     * constructor for photo class
     * @param photoName, name of the photo
     * @param path, file path to photo
     * @param caption, photo's caption
     */
    public Photo(String photoName, String path, String caption)
    {
        this.photoName = photoName;
        this.path = path;
        this.caption = caption;

        File f = new File(path);
        this.date = Calendar.getInstance();
        this.date.setTimeInMillis(f.lastModified());
        this.date.set(Calendar.MILLISECOND, 0);

        albumsIn = new ArrayList<Album>();
        photoTags = new ArrayList<Tag>();
    }

    /**
     * gets the name of the photo
     * @return photo name
     */
    public String getPhotoName()
    {
        return  this.photoName;
    }
    /**
     * get the path to photo
     * @return, file path
     */
    public String getPath()
    {
        return this.path;
    }

    /**
     * get caption of photo
     * @return caption
     */
    public String getCaption()
    {
        return this.caption;
    }

    /**
     * get date of photo
     * @return date of phtot
     */
    public Calendar getDate()
    {
        return date;
    }

    /**
     * get list of albums photo is in
     * @return list of albums
     */
    public ArrayList<Album> getAlbumsIn()
    {
        return albumsIn;
    }

    /**
     * get list of tags of photo
     * @return tag list
     */
    public ArrayList<Tag> getPhotoTags()
    {
        return photoTags;
    }

    /**
     * sets name of the photo
     * @param photoName, name of the photo
     */
    public void setPhotoName(String photoName)
    {
        this.photoName = photoName;
    }
    /**
     * set caption of photo
     * @param caption, caption of photo
     */
    public void setCaption(String caption)
    {
        this.caption = caption;
    }

    /**
     * add a tag to photo
     * @param tag, tag to be added
     */
    public void addTag(Tag tag)
    {
        if(this.photoTags.isEmpty())
        {
            photoTags.add(tag);
        }
        else
        {
            for (Tag t : photoTags)
            {
                if (t.equals(tag))
                {
                    Backend.errorPrompt("Tag error!", "The photo already has this tag!");
                    return;
                }
            }
            photoTags.add(tag);
        }
        Collections.sort(this.photoTags);
    }

    /**
     * removes a tag from the photo
     * @param tag, tag to be removed
     */
    public void removeTag(Tag tag)
    {
        if(this.photoTags.isEmpty())
        {
            return;
        }
        else
        {
            for (int i = 0; i < photoTags.size(); i++)
            {
                if (photoTags.get(i).equals(tag))
                {
                    photoTags.remove(i);
                    return;
                }
            }
        }
        Backend.errorPrompt("Tag error!", "The tag does not exist for this photo!");
    }

    /**
     * checks if album as indicated tag
     * @param tag, tag to be checked
     * @return true if photo has tag, false otherwise
     */
    public boolean checkTag(Tag tag)
    {
        for(Tag t : photoTags)
        {
            if(t.equals(tag))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * overrids equals method to check photo equivalence by checking file path
     * @param o, photo object to be checked
     * @return true if equal, false otherwise
     */
    @Override
    public boolean equals(Object o)
    {
        //make sure it is photo instance
        if (o == null || !(o instanceof Photo))
        {
            return false;
        }
        //cast to photo
        Photo op = (Photo) o;

        //check paths
        return this.getPath().equals(op.getPath());
    }
    /**
     * overrides the toString method
     */
    @Override
    public String toString() {
    	return photoName;
    }
    /**
     * overrides the compareTo method
     */
    @Override
    public int compareTo(Photo p)
    {
        return this.date.compareTo(p.getDate());
    }
}
