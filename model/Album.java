package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

public class Album implements Serializable, Comparator<Album>, Comparable<Album>
{
    //static final long serialVersionUID = 1L;

    /**
     * list of photos in album
     */
    private ArrayList<Photo> photoList;
    /**
     * name of album
     */
    private String albumName;
    /**
     * user that owns this album
     */
    private User user;
    /**
     * number of photos in album
     */
    private int numPhotos;
    /**
     * most recent photo date in the album
     */
    private Calendar newestDate;
    /**
     * oldest photo date in the album
     */
    private Calendar oldestDate;

    /**
     * constructor for Album
     * @param name, name of album
     * @param user, user that owns the album
     */
    public Album(String name, User user)
    {
        this.albumName = name;
        this.user = user;
        photoList = new ArrayList<Photo>();
        numPhotos = 0;
        this.oldestDate = null;
        this.newestDate = null;
    }

    /**
     * add a photo to the album
     * @param photo, photo to be added
     */
    public void addPhoto (Photo photo)
    {
        //check for null list
        if (photoList == null)
        {
            //initialize
            photoList = new ArrayList<Photo>();
            //add photo to list
            photoList.add(photo);
        }
        //check list if photo already exists
        for (Photo p : photoList)
        {
            if (p.equals(photo))
            {
                //prompt error to user
                Backend.errorPrompt("Add Photo Error", "Photo is already in this album!");
                return;
            }
        }
        //photo does not exist, add it
        this.photoList.add(photo);
        numPhotos++;

        // adjust newest and oldest photo dates
        if (oldestDate == null || photo.getDate().compareTo(oldestDate) < 0)
        {
            setOldestDate(photo.getDate());
        }

        if (newestDate == null || photo.getDate().compareTo(newestDate) > 0)
        {
            setNewestDate(photo.getDate());
        }
    }

    /**
     * delete a photo from the album
     * @param photo, photo to be deleted
     */
    public void deletePhoto(Photo photo)
    {
        //search list for photo
        for(Album a : photo.getAlbumsIn())
        {
            //if it is in the album, remove it
            if(this.albumName.equals(a.getAlbumName()))
            {
                photoList.remove(photo);
                numPhotos--;
                return;
            }
        }
        //does not exist in list, prompt user
        Backend.errorPrompt("Photo Delete Error", "Photo does not exist in album: " + this.albumName + "!");
    }

    /**
     * searches for a photo
     * @param photoName, name of photo being searched
     * @return photo if found
     */
    public Photo getPhoto(String photoName)
    {
        //check for null list
        if(photoList == null)
        {
            Backend.errorPrompt("Finding Photo Error", "Album is empty!");
            return null;
        }
        //search list by photo name
        for(Photo p : photoList)
        {
            if(p.getPhotoName().equals(photoName))
            {
                return p;
            }
        }
        //photo does not exist, prompt user
        Backend.errorPrompt("Finding Photo Error", "Photo is not in album: " + this.albumName + "!");
        return null;
    }

    /**
     * set the name of the album
     * @param albumName, name of the album
     */
    public void setAlbumName(String albumName)
    {
        this.albumName = albumName;
    }

    /**
     * set a user to the album
     * @param user, user to be set
     */
    public void setUser(User user)
    {
        this.user = user;
    }

    /**
     * set oldest photo date in album
     * @param oldestDate, date to be set
     */
    public void setOldestDate(Calendar oldestDate)
    {
        this.oldestDate = oldestDate;
    }

    /**
     * set newest photo date in album
     * @param newestDate, date to be set
     */
    public void setNewestDate(Calendar newestDate)
    {
        this.newestDate = newestDate;
    }

    /**
     * get name of album
     * @return the album name
     */
    public String getAlbumName()
    {
        return this.albumName;
    }

    /**
     * returns earliest photo date
     * @return  earliest photo date
     */
    public Calendar getNewestDate()
    {
        return this.newestDate;
    }

    /**
     * get oldest photo date
     * @return oldest photo date
     */
    public Calendar getOldestDate()
    {
        return this.oldestDate;
    }
    /**
     * get number of photos in album
     * @return number of photos
     */
    public int getNumPhotos()
    {
        return numPhotos;
    }
    /**
     * set the num of photos for the album
     * @param num
     */
    public void setNumPhotos(int num) {
    	numPhotos = num;
    }
    /**
     * get the user that owns the album
     * @return the user
     */
    public User getUser()
    {
        return this.user;
    }
    
    /**
     * @return the ArrayList of Photos
     */
    public ArrayList<Photo> getPhotoList() {
    	return photoList;
    }
    /**
     * compares album name
     * @param a, other album
     * @return return value of string's compareTo
     */
    public int compareTo(Album a)
    {
        return this.albumName.compareTo(a.getAlbumName());
    }

    /**
     * compares two albums by name
     * @param a, album
     * @param b, album
     * @return return from compareTo
     */
    public int compare(Album a, Album b)
    {
        return a.getAlbumName().compareTo(b.getAlbumName());
    }

    @Override
    public String toString()
    {
        return getAlbumName();
    }
}