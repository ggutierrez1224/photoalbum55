package controller;

import app.PhotoAlbum;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Backend;
import model.User;

import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Gutierrez
 *
 */
public class LogInController
{
    private Stage prevStage;

    @FXML
    TextField userNameInput;
    @FXML
    TextField passwordInput;
    @FXML
    Text txt;
    @FXML
    Button login;
    @FXML
    Button cancel;

   private ArrayList<User> users;

    @FXML
    public void initialize()
    {
        users = Backend.getUsers();
        // model.Backend.loadProject(); // Main app should load the project. Controllers should worry about
    	// sending commands for saving on exit.
    }

    /**
     * validates username and password before logging user in
     * @param event, login button was clicked
     */
    public void validateLogin(ActionEvent event) throws IOException
    {
       // model.Backend.loadProject();

        //check if admin is logging in
        if(userNameInput.getText().trim().equals("admin") && passwordInput.getText().trim().equals("admin"))
        {
            openAdminSystem();
        }
        //check if a user is logging in
        else if (validCredentials(userNameInput.getText().trim(), passwordInput.getText().trim()))
        {
            openUserSystem(userNameInput.getText().trim());
        }
        //incorrect input
        else
        {
            txt.setText("Invalid username/password.");
        }
    }

    /**
     * close log in window with cancel button
     * @param event, cancel button
     */
    public void cancel(ActionEvent event)
    {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    /**
     * opens admin sub system window
     */
    public void openAdminSystem()throws IOException
    {
        Stage stage = (Stage) login.getScene().getWindow();
        stage.setTitle("Admin System");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdminSystem.fxml"));
        Parent root = loader.load();
       
        prevStage = (Stage) login.getScene().getWindow();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * opens user system window
     */
    public void openUserSystem(String userName) throws IOException
    {
        Stage stage = (Stage)login.getScene().getWindow();
        stage.setTitle(userName);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserSystem.fxml"));
        Parent root = loader.load();
        UserSystemController controller = loader.getController();
        controller.setAlbumDetails(null);
        controller.setUser(userNameInput.getText().trim());
        prevStage = (Stage) login.getScene().getWindow();
        controller.setPrevStage(prevStage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * Checks if the user inputs valid credentials
     * @param name, username
     * @param password, user's password
     * @return true if provided credentials match list item, false otherwise
     */
    public boolean validCredentials(String name, String password)
    {
        //search through users list in Backend.
        for(User u: model.Backend.users)
        {
            //check if username and password match an entry in the list
            if(u.getUsername().equals(name) && password.equals(u.getPassword()))
            {
                    return true;
            }
        }
        //user input login is incorrect
        return false;
    }
}