package controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Backend;
import model.Photo;
import model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class AlbumOptionsController
{

    private static Stage prevStage;
    private static Stage thisStage;
    private static Album thisAlbum;
    private static ArrayList<Photo> photoList;
    private static User currUser;
    private Photo selectedPhoto;
    private int currIndex;

    @FXML
    TilePane tile;
    @FXML
    ScrollPane scroll;
    @FXML
    Label caption;
    @FXML
    Label albumLabel;
    @FXML
    Button addButton;
    @FXML
    Button deleteButton;
    @FXML
    Button displayButton;
    @FXML
    Button captionButton;
    @FXML
    Button closeAlbumButton;
    @FXML
    Button logOutButton;
    @FXML
    Button exitButton;
    @FXML
    Button moveTo;
    @FXML
    Button copyTo;
    @FXML
    Button addTag;
    @FXML
    Button deleteTag;
    @FXML
    TextField addTagFieldType;
    @FXML
    TextField addTagFieldValue;
    @FXML
    TextField deleteTagFieldType;
    @FXML
    TextField deleteTagFieldValue;
    @FXML
    TextField captionField;
    @FXML
    TextField moveField;
    @FXML
    TextField copyField;

    ObservableList<Photo> imgList;
    ObservableList<String> captionList;

    @FXML
    public void initialize()
    {
        tile.setPrefSize(scroll.getPrefWidth(), scroll.getPrefHeight());
        caption.setText("Caption");
    }

    /**
     * Updates the photo view with all of the photos in the current open album.
     * Also attaches on-click handlers for when a photo is clicked on.
     */
    private void updatePhotoView()
    {
        tile.getChildren().clear();
        for(Photo photo: thisAlbum.getPhotoList())
        {
            ImageView imageView = new ImageView(new Image(new File(photo.getPath()).toURI().toString(), 110, 110, false, true));
            imageView.setUserData(photo);
            imageView.setFitWidth(110);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
            imageView.setFocusTraversable(true);
            tile.getChildren().add(imageView);
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>(){
            
                @Override
                public void handle(MouseEvent arg0)
                {
                    for (Node n : tile.getChildren())
                    {
                        n.setEffect(null);
                    }
                    imageView.setEffect(new DropShadow(20, Color.DODGERBLUE));
                    selectedPhoto = (Photo) imageView.getUserData();
                    currIndex = photoList.indexOf(selectedPhoto);
                    caption.setText(selectedPhoto.getCaption());
                }
            });
        }
    }
    /**
     * Add a new photo. Popup dialogue to select photo location
     * @param event add button clicked
     * @throws IOException
     */
    public void add(ActionEvent event) throws IOException
    {
        String filePath = null;
        // File chooser from JavaFX
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a photo");
        File selectedFile = fileChooser.showOpenDialog((Stage) addButton.getScene().getWindow());
        if (selectedFile != null)
        {
            filePath = selectedFile.getAbsolutePath();
        }
        else
        {
            Backend.errorPrompt("File Error", "File is not valid!");
        }
        // Now using the absolute path, create a new photo object in the album.
        Calendar date = null;
        Photo newPhoto = new Photo("name", filePath, "caption");
        newPhoto.getAlbumsIn().add(thisAlbum);
        thisAlbum.addPhoto(newPhoto);

        updatePhotoView();
    }
    /**
     * delete the selected photo from the album
     * @param event delete button clicked
     * @throws IOException
     */
    public void delete(ActionEvent event) throws IOException
    {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	}
        thisAlbum.deletePhoto(selectedPhoto);
        updatePhotoView();
    }
    /**
     * add a caption to the selected photo
     * @param event caption button clicked
     * @throws IOException
     */
    public void caption(ActionEvent event) throws IOException {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	} else if (captionField.getText().trim().equals("")) {
    		Backend.errorPrompt("Error", "no caption entered");
    		return;
    	} else {
    		selectedPhoto.setCaption(captionField.getText());
    		caption.setText(captionField.getText());
    		captionField.clear();
    		updatePhotoView();
    	}
    }
    /**
     * Display photo in PhotoDisplay
     * @param event the display button is clicked
     * @throws IOException
     */
    public void display(ActionEvent event) throws IOException {
    	// Lets change the scene.
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoDisplay.fxml"));
        Parent root = loader.load();
        PhotoDisplayController controller = (PhotoDisplayController) loader.getController();
        controller.setThePhoto(selectedPhoto);
        controller.setPrevScene(thisStage.getScene());
        controller.setTheStage(thisStage);
        controller.setPhotoList(photoList);
        Scene scene = new Scene(root);
        thisStage.setScene(scene);
    }
    /**
     * move the photo to the album specified in the text field
     * @param event
     * @throws IOException
     */
    public void moveTo(ActionEvent event) throws IOException {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	}
    	// does the album even exist?
    	String albumName = moveField.getText().trim();
    	moveField.clear();
    	boolean exists = false;
    	for (Album a: currUser.getAlbums()) {
    		if(a.getAlbumName().compareTo(albumName) == 0) {
    			exists = true;
    			if (exists == true) {
    	    		a.addPhoto(selectedPhoto);
    	    		if (a == thisAlbum) {
    					break;
    				}
    	    		thisAlbum.getPhotoList().remove(selectedPhoto);
    	    		thisAlbum.setNumPhotos(thisAlbum.getNumPhotos() - 1);
    	    		updatePhotoView();
    	    		break;
    	    	}
    		}
    	}
    	if (exists == false) {
    		Backend.errorPrompt("Doesn't Exist", "That album doesn't exist");
    	}	
    }
    /**
     * copy the photo to the album specified in the text field
     * @param event
     * @throws IOException
     */
    public void copyTo(ActionEvent event) throws IOException {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	}
    	// does the album even exist?
    	String albumName = copyField.getText().trim();
    	copyField.clear();
    	boolean exists = false;
    	for (Album a: currUser.getAlbums()) {
    		if(a.getAlbumName().compareTo(albumName) == 0) {
    			exists = true;
    			if (exists == true) {
    	    		a.addPhoto(selectedPhoto);
    	    		updatePhotoView();
    	    		break;
    	    	}
    		}
    	}
    	if (exists == false) {
    		Backend.errorPrompt("Doesn't Exist", "That album doesn't exist");
    	}	
    }
    /**
     * add tag to selected photo
     * @param event add tag button clicked
     * @throws IOException
     */
    public void addTag(ActionEvent event) throws IOException {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	} else if (addTagFieldType.getText().trim().equals("")) {
    		Backend.errorPrompt("Error", "No Tag Type");
    		return;
    	} else if (addTagFieldValue.getText().trim().equals("")) {
    		Backend.errorPrompt("Error", "No Tag Value");
    		return;
    	} else {
    		selectedPhoto.addTag(new model.Tag(addTagFieldType.getText(), addTagFieldValue.getText()));
    		updatePhotoView();
    	}
    	addTagFieldType.clear();
    	addTagFieldValue.clear();
    }
    /**
     * remove tag from selected photo
     * @param event delete tag button clicked
     * @throws IOException
     */
    public void deleteTag(ActionEvent event) throws IOException {
    	if (selectedPhoto == null) {
    		Backend.errorPrompt("Error", "No Photo Selected");
    		return;
    	} else if (deleteTagFieldType.getText().trim().equals("")) {
    		Backend.errorPrompt("Error", "No Tag Type");
    		return;
    	} else if (deleteTagFieldValue.getText().trim().equals("")) {
    		Backend.errorPrompt("Error", "No Tag Value");
    		return;
    	} else {
    		selectedPhoto.removeTag(new model.Tag(deleteTagFieldType.getText(), deleteTagFieldValue.getText()));
    		updatePhotoView();
    	}
    	deleteTagFieldType.clear();
    	deleteTagFieldValue.clear();
    }
    /**
     * close the album options
     * @param event close button clicked
     * @throws IOException
     */
    public void closeAlbum(ActionEvent event) throws IOException
    {
        Backend.save();

        Stage stage = (Stage)closeAlbumButton.getScene().getWindow();
        stage.setTitle(currUser.getUsername());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserSystem.fxml"));
        Parent root = loader.load();
        UserSystemController controller = loader.getController();

        controller.setUser(currUser.getUsername());
        prevStage = (Stage) closeAlbumButton.getScene().getWindow();
        controller.setPrevStage(prevStage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    /**
     * logout of user
     * @param event logout button clicked
     * @throws IOException
     */
    public void logOut(ActionEvent event) throws IOException {
        Backend.save();

        Stage stage = (Stage)logOutButton.getScene().getWindow();
        stage.setTitle("Log In");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root, 300, 250);
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }
    /**
     * exit from applicaton
     * @param event exit button clicked
     * @throws IOException
     */
    public void exit(ActionEvent event) throws IOException {
        // Here we must save the data.
        Backend.save(); // Send a call to backend to save.
        Platform.exit();
    }
    /**
     * set the album for the controller
     * @param a album
     */
    public void setAlbum(Album a)
    {
        thisAlbum = a;

        albumLabel.setText(thisAlbum.getAlbumName());
    }
    /**
     * set the user for the controller
     * @param u user
     */
    public void setUser(User u)
    {
        currUser = u;
    }
    /**
     * set the list of photos for the album options controller
     */
    public void setPhotos()
    {
        photoList = thisAlbum.getPhotoList();

        updatePhotoView();
    }
    /**
     * set the previous stage for the album options controller
     * @param stage
     */
    public void setPreviousStage(Stage stage) {
    	prevStage = stage;
    }
    public void setThisStage(Stage stage) {
    	thisStage = stage;
    }
}
