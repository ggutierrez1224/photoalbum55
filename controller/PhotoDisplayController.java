package controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Photo;
import model.Tag;

/**
 * 
 * @author Lyle Filonuk
 *
 */
public class PhotoDisplayController {
	
	private model.Photo thePhoto;
	private Stage theStage;
	private Scene prevScene;
	private ArrayList<Photo> photoList;
	
	@FXML
	Button close;
	@FXML
	Button next;
	@FXML
	Button previous;
	@FXML
	ImageView imageView;
	@FXML
	TextArea dateText = new TextArea();
	@FXML
	TextArea captionText = new TextArea();
	@FXML
	TextArea tagsTextArea = new TextArea();
	
	@FXML
    public void initialize()
    {
    }
	/**
	 * close the scene and return to album options
	 * @param event click close button
	 * @throws IOException
	 */
	public void close(ActionEvent event) throws IOException {
		theStage.setScene(prevScene);
	}
	/**
	 * display the next photo in the album
	 * @param event the next button is selected
	 * @throws IOException
	 */
	public void next(ActionEvent event) throws IOException {
		int index = photoList.indexOf(thePhoto);
		index++;
		if (index == photoList.size()){
			index = 0; // reset the index so we can go back to the beginning.
		}
		setThePhoto(photoList.get(index));
	}
	/**
	 * display the previous photo in the album
	 * @param event previous photo button selected
	 * @throws IOException
	 */
	public void previous(ActionEvent event) throws IOException {
		int index = photoList.indexOf(thePhoto);
		if (index == 0) {
			index = photoList.size(); // reset the index so we can go back to the beginning.
		}
		index--;
		setThePhoto(photoList.get(index));
	}
	/**
	 * the photo currently being displayed
	 * @param photo a Photo
	 */
	public void setThePhoto(model.Photo photo) {
		thePhoto = photo;
        imageView.setImage(new Image(new File(thePhoto.getPath()).toURI().toString(), false));
        imageView.setPreserveRatio(true);
        Image theImage = imageView.getImage();
        centerImage(theImage, imageView);
        
        dateText.setText(thePhoto.getDate().getTime().toString());
        captionText.setText(thePhoto.getCaption());
        String tagsString = "";
        for (Tag t: thePhoto.getPhotoTags()) {
        	tagsString += t.getType() + "," + " " + t.getValue() + "\n";
        }
        tagsTextArea.setText(tagsString);
	}
	/**
	 * the scene for the album options
	 * @param scene
	 */
	public void setPrevScene(Scene scene) {
		prevScene = scene;
	}
	/**
	 * the stage for the photo display
	 * @param stage
	 */
	public void setTheStage(Stage stage) {
		theStage = stage;
	}
	/**
	 * the photo list of the album
	 * @param photoList ArrayList of photos
	 */
	public void setPhotoList(ArrayList<Photo> photoList) {
		this.photoList = photoList;
	}
	/**
	 * center the image
	 * @param img Image
	 * @param imgView ImageView
	 */
	public void centerImage(Image img, ImageView imgView) {
        if (img != null) {
            double w = 0;
            double h = 0;

            double ratioX = imageView.getFitWidth() / img.getWidth();
            double ratioY = imageView.getFitHeight() / img.getHeight();

            double reducCoeff = 0;
            if(ratioX >= ratioY) {
                reducCoeff = ratioY;
            } else {
                reducCoeff = ratioX;
            }

            w = img.getWidth() * reducCoeff;
            h = img.getHeight() * reducCoeff;

            imageView.setX((imageView.getFitWidth() - w) / 2);
            imageView.setY((imageView.getFitHeight() - h) / 2);

        }
    }
}
