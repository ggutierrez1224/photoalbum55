package controller;

import app.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Gabriel Gutierrez
 *
 */
public class UserSystemController
{
    private static ArrayList<User> users;
    private static User user;
    private static Stage prevStage;
    private ObservableList<Album> obsList;

    @FXML
    ListView<Album> albList = new ListView<Album>();
    @FXML
    Button createAlb;
    @FXML
    Button openAlb;
    @FXML
    Button deleteAlb;
    @FXML
    Button renameAlb;
    @FXML
    Button logout;
    @FXML
    Button exit;
    @FXML
    Button searchD;
    @FXML
    Button searchT;
    @FXML
    TextField albName;
    @FXML
    TextField dateStart;
    @FXML
    TextField dateEnd;
    @FXML
    TextField tagType;
    @FXML
    TextField tagValue;
    @FXML
    Label aName;
    @FXML
    Label numPhotos;
    @FXML
    Label recent;
    @FXML
    Label oldest;

    @FXML
    private void initialize()
    {
        Backend.loadProject();
        setAlbList();
        setAlbumDetails(null);
    }

    /**
     * updates list view
     */
    public void setAlbList()
    {
        if(obsList != null)
        {
            obsList.clear();
        }
        if(user != null && user.getAlbums() != null )
        {
            //System.out.println("here");
            obsList = FXCollections.observableArrayList(user.getAlbums());
            FXCollections.sort(obsList);
            albList.setItems(obsList);
            albList.requestFocus();
            albList.getSelectionModel().select(0);
        }
        if(obsList == null)
        {
            albList.setPlaceholder(new Label("No Albums In List"));
        }
    }

    /**
     * create button was clicked
     * @param event, create button
     */
    public void createAlbum(ActionEvent event)
    {
        user.addAlbum(albName.getText());
        setAlbList();

        if (albList != null)
        {
            albList.getSelectionModel().select(0);
            setAlbumDetails(user.getAlbum(albList.getSelectionModel().getSelectedItem().getAlbumName()));
        }
        albName.clear();
    }

    public void albumClick(MouseEvent event)
    {
        if(albList.getSelectionModel().getSelectedItem() != null)
        {
            setAlbumDetails(albList.getSelectionModel().getSelectedItem());
        }
    }

    /**
     * delete button was clicked
     * @param event, delete button
     */
    public void deleteAlbum(ActionEvent event)
    {
        if(Backend.confirm("Delete Album", "Delete: " + albList.getSelectionModel().getSelectedItem().getAlbumName() + "?"))
        {
            user.removeAlbum(albList.getSelectionModel().getSelectedItem());
            setAlbList();
        }

        if (albList != null && !obsList.isEmpty())
        {
            albList.getSelectionModel().select(0);
            setAlbumDetails(user.getAlbum(albList.getSelectionModel().getSelectedItem().getAlbumName()));
        }
    }

    /**
     *renames selected album
     * @param event, rename button
     */
    public void renameAlb(ActionEvent event)
    {
        String currName = albList.getSelectionModel().getSelectedItem().getAlbumName();
        String newName = Backend.inputPrompt("Rename Album" ,"Current Name: "
                + currName , "New name:");
        if(newName != null)
        {
            if(newName.trim().isEmpty())
            {
                Backend.errorPrompt("Empty Input", "Album name can't be set to empty string!");
            }
            else
            {
                if(user.checkName(newName)  && !newName.equals(currName))
                {
                    Backend.errorPrompt("New Name Conflict", "New name is already used!");
                }
                else if(newName.equals(currName))
                {
                    Backend.errorPrompt("New Name Conflict", "You can't rename with the same name!");
                }
                else
                {
                    user.getAlbum(currName).setAlbumName(newName);
                    setAlbList();
                }
            }
        }
    }

    public void openAlbum(ActionEvent event) throws IOException
    {
        Stage stage = new Stage();
        stage.setTitle(albList.getSelectionModel().getSelectedItem().getAlbumName());

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumOptions.fxml"));
        Parent root = loader.load();

        AlbumOptionsController controller = (AlbumOptionsController) loader.getController();
        controller.setAlbum(albList.getSelectionModel().getSelectedItem());
        controller.setUser(user);
        controller.setPhotos();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        prevStage.close();

        stage.show();
    }

    /**
     * searches user albums for photos meeting search criteria
     * then creates an array list of photos found and passes them to the next window
     * @param event
     */
    public void searchByDate(ActionEvent event) throws IOException
    {
        if (dateStart.getText().equals("") || dateEnd.getText().equals(""))
        {
            Backend.errorPrompt("Search Error", "Missing input!");
            return;
        }

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
        Date sd;
        Date ed;
        Calendar startDate = Calendar.getInstance();
        Calendar endDate = Calendar.getInstance();

        ArrayList<Photo> results = new ArrayList<Photo>();
        try
        {
            sd = format.parse(dateStart.getText());
            ed = format.parse(dateEnd.getText());
            startDate.setTime(sd);
            endDate.setTime(ed);
        }
        catch (ParseException e)
        {
            Backend.errorPrompt("Search Error", "Please enter date as mm/dd/yy.");
            return;
        }
        if (startDate.compareTo(endDate) > 0 || startDate.equals(endDate))
        {
            Backend.errorPrompt("Search Error", "Start date must be before end date!");
            return;
        }
        for (Album a : user.getAlbums())
        {
            for (Photo p : a.getPhotoList())
            {
                if (p.getDate().compareTo(startDate) >= 0 && p.getDate().compareTo(endDate) <= 0)
                {
                    results.add(p);
                }
            }
        }
        if (results.size() == 0)
        {
            Backend.errorPrompt("Search Error", "No photos found!");
        }
        else
        {
            Backend.save();

            Stage stage = new Stage();
            stage.setTitle(albList.getSelectionModel().getSelectedItem().getAlbumName());

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SearchResults.fxml"));
            Parent root = loader.load();

            SearchResultsController controller = (SearchResultsController) loader.getController();
            controller.setPhotoList(results);
            controller.setUser(user);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            prevStage.close();

            stage.show();
        }

    }

    /**
     * searches user albums for photos meeting search criteria
     * then creates an array list of photos found and passes them to the next window
     * @param event
     */
    public void searchByTag(ActionEvent event) throws IOException
    {
        //save input
        String tType = tagType.getText().trim();
        String tValue = tagValue.getText().trim();
        ArrayList<Photo> results = new ArrayList<Photo>();

        //tags have two elements, type and value
        if(tType.isEmpty() || tValue.isEmpty())
        {
            Backend.errorPrompt("Search Error", "Missing input!");
        }
        else
        {
            //got through each album, and search each list of photos checking their tags with the input tag
            for(Album a : user.getAlbums())
            {
                for(Photo p : a.getPhotoList())
                {
                    Tag t = new Tag(tType, tValue);
                    //add photo to list if it has input tag
                    if(p.checkTag(t))
                    {
                        results.add(p);
                    }
                }
            }
        }
        //open searchResults window if list is not empty
        if(!results.isEmpty())
        {
            Backend.save();

            Stage stage = new Stage();
            stage.setTitle(albList.getSelectionModel().getSelectedItem().getAlbumName());

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SearchResults.fxml"));
            Parent root = loader.load();

            SearchResultsController controller = (SearchResultsController) loader.getController();
            controller.setPhotoList(results);
            controller.setUser(user);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            prevStage.close();

            stage.show();
        }
        //prompt user of empty search results
        else
        {
            Backend.errorPrompt("Search Error", "No photos found!");
        }
    }

    /**
     * logs user out and returns to log in window
     * @param event, log out button
     */
    public void logout(ActionEvent event) throws IOException
    {
        Backend.save();

        Stage stage = (Stage)logout.getScene().getWindow();
        stage.setTitle("Log In");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root, 300, 250);
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    /**
     * saves and axits application
     * @param event
     */
    public void exitSystem(ActionEvent event)
    {
        Backend.save();
        prevStage.close();
    }

    /**
     * fills in the text area with selected album details
     * @param a, selected album
     */
    public void setAlbumDetails(Album a)
    {
        if(a == null)
        {
            aName.setText("N/A");
            numPhotos.setText("N/A");
            recent.setText("N/A");
            oldest.setText("N/A");
        }
        else
        {
            aName.setText(a.getAlbumName());
            numPhotos.setText(a.getNumPhotos() + "");
            if(a.getNewestDate() == null)
            {
                recent.setText("N/A");
            }
            else
            {
                recent.setText(a.getNewestDate().getTime() + "");
            }
            if(a.getOldestDate() == null)
            {
                oldest.setText("N/A");
            }
            else
            {
                oldest.setText(a.getOldestDate().getTime() + "");
            }
        }
    }

    /**
     * sets the current logged in user
     * @param username, username of current user
     */
    public void setUser(String username)
    {
        if(users == null)
        {
            users = Backend.getUsers();
        }
        for (User u : users)
        {
            if (u.getUsername().equals(username))
            {
                user = u;
            }
        }
        setAlbList();
    }

    /**
     * keeps track of previous stage
     * @param stage, previous stage
     */
    public void setPrevStage(Stage stage)
    {
        this.prevStage = stage;
    }

}
