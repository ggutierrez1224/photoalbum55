package controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import model.Album;
import model.Backend;
import model.Photo;
import model.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Gutierrez
 *
 */
public class SearchResultsController
{
    /**
     * current user
     */
    private static User user;
    /**
     * list of photos from search
     */
    private static ArrayList<Photo> photoList;
    /**
     * current selected photo
     */
    private Photo selectedPhoto;
    /**
     * previous stage
     */
    public static Stage prevStage;

    @FXML
    TextField otherAlb;
    @FXML
    Button create;
    @FXML
    Button logout;
    @FXML
    Button exit;
    @FXML
    Button close;
    @FXML
    ScrollPane scroll;
    @FXML
    TilePane tile;
    @FXML
    Label caption;

    @FXML
    public void initialize()
    {
        caption.setText("N/A");
    }

    /**
     * Updates the photo view with all of the photos in the current open album.
     * Also attaches on-click handlers for when a photo is clicked on.
     */
    private void updatePhotoView()
    {
        tile.getChildren().clear();
        // System.out.println("updating photo view");
        for(Photo photo: photoList)
        {
            ImageView imageView = new ImageView(new Image(new File(photo.getPath()).toURI().toString(), 110, 110, false, true));
            imageView.setUserData(photo);
            imageView.setFitWidth(110);
            imageView.setPreserveRatio(true);
            imageView.setSmooth(true);
            imageView.setCache(true);
            imageView.setFocusTraversable(true);
            tile.getChildren().add(imageView);
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>(){

                @Override
                public void handle(MouseEvent arg0)
                {
                    for (Node n : tile.getChildren())
                    {
                        n.setEffect(null);
                    }
                    imageView.setEffect(new DropShadow(20, Color.DODGERBLUE));
                    selectedPhoto = (Photo) imageView.getUserData();
                    caption.setText(selectedPhoto.getCaption());
                    // System.out.println(selectedPhoto);
                }

            });
        }
    }

    /**
     * creates new album out of search results
     * @param event
     */
    public void createAlbum(ActionEvent event)
    {
        //create album
        String name = otherAlb.getText().trim();

        //if album was doesn't exits, store search results to new album
        if(!user.checkName(name))
        {
            user.getAlbum(name);
            for(Photo p : photoList)
            {
                user.getAlbum(name).addPhoto(p);
            }
        }
    }

    /**
     * logs user out and returns to log in window
     * @param event, log out button
     */
    public void logout(ActionEvent event) throws IOException
    {
        Backend.save();

        Stage stage = (Stage)logout.getScene().getWindow();
        stage.setTitle("Log In");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root, 300, 250);
        stage.setScene(scene);
        stage.show();
        stage.setResizable(false);
    }

    /**
     * closes search results window and returns to the user system window
     * @param event
     * @throws IOException
     */
    public void closeWindow(ActionEvent event) throws IOException
    {
        Backend.save();

        Stage stage = (Stage)close.getScene().getWindow();
        stage.setTitle(user.getUsername());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserSystem.fxml"));
        Parent root = loader.load();
        UserSystemController controller = loader.getController();

        controller.setUser(user.getUsername());
        prevStage = (Stage) close.getScene().getWindow();
        controller.setPrevStage(prevStage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * saves and exits application
     * @param event
     */
    public void exitSystem(ActionEvent event)
    {
        Backend.save(); // Send a call to backend to save.
        Platform.exit();
    }

    /**
     * sets current user
     * @param u current user
     */
    public void setUser(User u)
    {
        user = u;
    }

    /**
     * sets the list of photo results
     * @param list result of search
     */
    public void setPhotoList(ArrayList<Photo> list)
    {
        photoList = list;
        updatePhotoView();
    }
}