package controller;

import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;


/**
 * This class brokers between the fxml display and Backend.java.
 * 
 * @author Lyle Filonuk
 *
 */
public class AdminSystemController {
			
	@FXML
	ListView<String> adminDisplay;
	@FXML
	TextField nameInput;
	@FXML
	Button logOutButton;
	@FXML
	Button createButton;
	@FXML
	Button deleteButton;
	
	ObservableList<String> obsList;
	
	// Our ititializer for this controller. It loads the screen.
	@FXML
	public void initialize() {
		obsList = FXCollections.observableArrayList();
		// Let's load the users from the backend.
		if (model.Backend.users == null) {
			return;
		}
		for(int i = 0; i < model.Backend.users.size(); i++) {
				obsList.add(model.Backend.users.get(i).getUsername());
		}
		adminDisplay.setItems(obsList);
	}
	
	// We're going to create a new user.
	public void create(ActionEvent event) throws IOException {
		String newUserName = nameInput.getText();
		model.Backend.addUser(newUserName);
		obsList.add(newUserName);
	}
	
	// We're going to delete a selected user.
	public void delete(ActionEvent event) throws IOException {
		int index = adminDisplay.getSelectionModel().getSelectedIndex();
		model.Backend.deleteUser(index);
		obsList.remove(index);
	}
	
	// Return to the login screen.
	public void logOut(ActionEvent event) throws IOException {
		model.Backend.save(); // Send a call to backend to save in case the user cancels afterward.
		Stage stage = (Stage)logOutButton.getScene().getWindow();
        stage.setTitle("Admin System");
        Parent root = FXMLLoader.load(getClass().getResource("/view/LogIn.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
	}
	
	// Exit the JavaFX application.
	public void exit(ActionEvent event) throws IOException {
		// Here we must save the data.
		model.Backend.save(); // Send a call to backend to save.
		Platform.exit(); 
	}
}
