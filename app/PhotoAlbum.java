package app;

import controller.LogInController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.Backend;
import model.User;

import java.io.*;
import java.util.ArrayList;

public class PhotoAlbum extends Application implements Serializable
{
    //static final long serialVersionUID = 1L;
    		
    @Override
    public void start(Stage primaryStage) throws Exception
    {
    	// Call the backend in the model package to tell it to load the data.
        Backend.loadProject();

        primaryStage.setTitle("Log In");
        primaryStage.setOpacity(0.8);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/LogIn.fxml"));
        //loader.setLocation(getClass().getResource("/view/LogIn.fxml"));
        AnchorPane root = (AnchorPane) loader.load();

        Scene scene = new Scene(root, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setResizable(false);
        
    }


    public static void main(String[] args)
    {
        launch(args);
    }
}